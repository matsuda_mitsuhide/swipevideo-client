const path = require('path');
const webpack = require('webpack');
const fs = require( "fs" );
// const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const OpenBrowserPlugin = require( "open-browser-webpack-plugin" );
const nodeExternals = require('webpack-node-externals');
const PRODUCTION = process.argv[ 2 ] === "-p";

if ( !PRODUCTION ) {
  const static = require( "node-static" );
  const file = new static.Server( "./www" );
  require( "http" ).createServer( ( request, response ) => {
    if ( /^\/contents_list\.json/.test( request.url ) ) {
      response.end( JSON.stringify( fs.readdirSync( "www/viewer/" ).filter( file => fs.statSync( "www/viewer/" + file ).isDirectory() ) ) );
    } else {
      request.addListener( "end", () => file.serve(request, response) ).resume();
    }
  }).listen( 80 );
}

module.exports = [{
  entry: {
    "build/swipevideo": "./src/swipevideo.js",
    "viewer/index": "./src/viewer/index.jsx"
  },
  output: {
    path: path.resolve('./www'),
    filename: "[name].js"
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        query: {
            comments: false,
            compact: true
        },
        exclude: /node_modules/
      },
      {
        test: /\.png$/,
        loader: 'url-loader',
        exclude: /node_modules/
      },
    ]
  },
  plugins: [
    PRODUCTION
    ? new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
        },
      })
    : new OpenBrowserPlugin({ url: "http://localhost/" })
  ]
    // : new BrowserSyncPlugin({
    //     proxy: "http://localhost/",
    //     port: 3333,
    //     open: "external"
    //   })
}, {
  target: 'node',
  externals: [ nodeExternals() ],
  entry: {
    "build/swipevideo.module": "./src/index.js"
  },
  output: {
    path: path.resolve('./'),
    filename: "[name].js",
    libraryTarget: "commonjs2"
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        query: {
            comments: false,
            compact: true
        },
        exclude: /node_modules/
      },
      {
        test: /\.png$/,
        loader: 'url-loader',
        exclude: /node_modules/
      },
    ]
  }
}];
