import { ImageBufferLoader } from "../helper.js"; 

module.exports = class {
  constructor( elem, store ) {
    this.elem = elem;
    this.store = store;
    this.dataList = [];
  }
  
  onImageLoad( loader ) {
    this.elem.src = loader.objectURL;
  }

  setLoader( state ) {
    const info = this.store.info;
    const swipedIndex = state.swipedIndex;
    
    if ( !this.dataList[ swipedIndex ] ) {
      this.dataList[ swipedIndex ] = new ImageBufferLoader(
        `./${ info.id }/${ info.images[ swipedIndex ] }/image/${ Math.floor( state.currentTime * info.fps ) + 1 }.jpg${ this.store.query_string ? "?" + this.store.query_string : "" }`,
        this.onImageLoad.bind( this )
      );
    } else if ( this.dataList[ swipedIndex ].objectURL ) {
      this.elem.src = this.dataList[ swipedIndex ].objectURL;
    }
  }

  update( state, prevState ) {
    if ( state.playing ) {
      if ( !prevState.playing ) this.trashData();
      return;
    }

    if ( prevState.playing ) {
      URL.revokeObjectURL( this.elem.src );
    } else if ( state.seekedTime !== prevState.seekedTime ) {
      this.trashData();
      this.setLoader( state );
    } else if ( state.swipedIndex !== prevState.swipedIndex ) {
      this.setLoader( state );
    }
  }

  trashData() {
    this.dataList.forEach( loader => URL.revokeObjectURL( loader.objectURL ) );
    this.dataList.length = 0;
  }

  canplay() {
    return true;
  }

  play( bool ) {
    return;
  }
};
