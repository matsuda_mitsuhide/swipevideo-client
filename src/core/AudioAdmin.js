module.exports = class {
  constructor( elem ) {
    this.elem = elem;
    
    this.canceler = null;
    this.prevCanPlayState = false;
    
    // 1秒ごとに音声とタイマーのずれを確認する
    this.adjustedTime = Date.now();
  }

  update( state, prevState ) {
    
    if ( state.seekedTime !== prevState.seekedTime ) {
      this.elem.currentTime = state.currentTime;
    } else if ( state.playing && Date.now() - this.adjustedTime >= 1000 ) {
      this.adjustedTime = Date.now();
      if ( Math.abs( this.elem.currentTime - state.currentTime ) > .1 ) {
        this.elem.currentTime = state.currentTime;
      }
    }
    
  }

  canplay() {
    // 0.5秒以上falseならfalseを返す
    if ( this.elem.readyState > this.elem.HAVE_CURRENT_DATA ) {
      clearTimeout( this.canceler );
      this.canceler = null;
      this.prevCanPlayState = true;
      return true;
    } else {
      if ( this.prevCanPlayState ) {
        if ( this.canceler === null ) {
          this.canceler = setTimeout( () => this.prevCanPlayState = false, 1000 );
        }
        
        return true;
      } else {
        return false;
      }
    }
  }

  play( bool ) {
    if ( bool ) {
      this.elem.paused && this.elem.play();
    } else {
      !this.elem.paused && this.elem.pause();
    }
  }
};
