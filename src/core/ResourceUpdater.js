import AudioAdmin from "./AudioAdmin.js";
import PlayingImageAdmin from "./PlayingImageAdmin.js";
import PausingImageAdmin from "./PausingImageAdmin.js";

// 再生行程に名前を付けて、各リソースのplay/pauseをbooleanで指定
const sequence = {
  LOADING: {
    audio: false,
    socketImage: false,
    pausingImage: false
  },
  SOCKET_LOADING: {
    audio: false,
    socketImage: true,
    pausingImage: false
  },
  PAUSING: {
    audio: false,
    socketImage: false,
    pausingImage: true
  },
  PAUSING_SWIPING: {
    audio: false,
    socketImage: false,
    pausingImage: true
  },
  SOCKET_WAITING: {
    audio: true,
    socketImage: true,
    pausingImage: false
  },
  SOCKET_PLAYING: {
    audio: true,
    socketImage: true,
    pausingImage: false
  }
};

module.exports = class {
  constructor( props ) {
    this.resources = {
      audio: new AudioAdmin( props.audio, props.store ),
      socketImage: new PlayingImageAdmin( props.image, props.store ),
      pausingImage: new PausingImageAdmin( props.image, props.store )
    };

    this.disposed = false;
    this.props = props;

    this.state = {};
    this.prevState = {};
    
    this.ended = false;
    this.canReplay = false;
    
    this.onEnded = () => {
      this.ended = true;
    };
    
    props.audio.addEventListener( "ended", this.onEnded, false );

    this.loadingFlag = false;

    requestAnimationFrame( this.update.bind( this ) );
  }

  setState( newState ) {
    this.state = Object.assign( {}, this.state, newState );

    if ( this.prevState.seekedTime !== this.state.seekedTime ) {
      this.state.currentTime = this.state.seekedTime;
    }
  }

  setCurrentTime() {
    this.state = Object.assign( {}, this.state, {
      currentTime: this.props.audio.currentTime
    });
  }

  update() {

    if ( this.disposed ) return;

    requestAnimationFrame( this.update.bind( this ) );
    
    //Replay
    if ( this.state.playing && this.canReplay ) {
      this.ended = false;
      this.canReplay = false;
      this.setState({
        currentTime: 0
      });
    }

    if ( this.ended || this.state.currentTime >= this.props.store.info.duration ) {
      Object.keys( this.resources ).forEach( key => this.resources[ key ].play( false ) );
      this.props.onEnded();
      this.canReplay = true;
      return;
    }

    const resources = this.resources;
    let sequenceStatus;

    Object.keys( resources ).forEach( key => resources[ key ].update( this.state, this.prevState ) );
    this.prevState = this.state;

    if ( this.state.playing ) {

      if ( resources.audio.canplay() && resources.socketImage.canplay() ) {
        sequenceStatus = sequence.SOCKET_PLAYING;
      } else {
        sequenceStatus = sequence.SOCKET_LOADING;
      }

    } else {

      if ( this.state.swiping ) {
        if ( !resources.pausingImage.canplay() ) {
          sequenceStatus = sequence.LOADING;
        } else {
          sequenceStatus = sequence.PAUSING_SWIPING;
        }
      } else {
        sequenceStatus = sequence.PAUSING;
      }
    }

    if ( sequenceStatus.audio ) {
      this.setCurrentTime();
    }

    const loadingFlag = sequenceStatus === sequence.LOADING
      || sequenceStatus === sequence.SOCKET_LOADING;
    
    if ( loadingFlag !== this.state.loading ) {
      this.props.onChangeLoading( loadingFlag );
    }

    Object.keys( resources ).forEach( key => resources[ key ].play( sequenceStatus[ key ] ) );

  }

  dispose() {
    this.disposed = true;
    props.audio.removeEventListener( "ended", this.onEnded, false );
  }
};
