import { ImageBufferLoader } from "../helper.js"; 

module.exports = class {
  constructor( elem, store ) {
    this.elem = elem;
    this.store = store;

    this.playing = false;
    this.delay = 0;

    // SocketClientから取得した画像オブジェクトのリスト
    this.dataList = [];

    // 現在の実効FPS
    this.fps = store.info.fps;
    this.intervalObject = null;
    this.loadedCounter = 0;
    this.fpsCountTimer = 0;

    this.currentTimeCache = 0;

  }
  
  onImageLoad( loader ) {
    const loaderIndex = this.dataList.indexOf( loader );
    
    if ( loaderIndex !== -1 ) {
      // -1の場合は遅れてる
      
      URL.revokeObjectURL( this.elem.src );
      this.elem.src = loader.objectURL;
    }
    this.dataList = this.dataList.slice( loaderIndex + 1 );
    this.loadedCounter++;
    this.delay = loader.delay / 1000;
  }

  update( state, prevState ) {
    if ( state.seekedTime !== prevState.seekedTime ) {
      this.currentTimeCache = state.currentTime - 1;
    } else if ( state.replayCounter !== prevState.replayCounter ) {
      // seekせずにreplayすると変化が無いので描画が止まってしまう
      this.currentTimeCache = -1;
    }

    const info = this.store.info;
    
    if ( !state.playing || this.delay + state.currentTime > info.duration || state.currentTime - this.currentTimeCache < .8 / this.fps ) return;

    this.currentTimeCache = state.currentTime;
    
    this.dataList.push(
      new ImageBufferLoader( `./${ info.id }/${ info.images[ state.swipedIndex ] }/image/${ Math.floor( ( this.delay + state.currentTime ) * info.fps ) + 1 }.jpg${ this.store.query_string ? "?" + this.store.query_string : "" }`, this.onImageLoad.bind( this ) )
    );

  }

  canplay() {
    return true;
  }

  play( bool ) {
    if ( bool ) {
      if ( this.playing || !this.canplay() ) return;
      this.playing = true;
      this.loadedCounter = 0;
      this.fpsCountTimer = Date.now();
      
      this.intervalObject = setInterval( () => {
        const now = Date.now();
        const fps = Math.floor( this.loadedCounter * 1000 / ( now - this.fpsCountTimer ) ) + 1;
        
        this.fps = Math.min( this.store.info.fps, fps );
        this.loadedCounter = 0;
        this.fpsCountTimer = now;

      }, 1000 );
    } else {
      if ( !this.playing ) return;
      this.playing = false;
      this.dataList.length = 0;
      clearInterval( this.intervalObject );
    }
  }
};
