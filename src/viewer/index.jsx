import React from "react";
import ReactDOM from "react-dom";
import Player from "../components/Player.jsx";

window.addEventListener( "message", e => {
  const param = e.data;
  
  switch( param.type ) {
  case "init":
    const xhr = new XMLHttpRequest;
    xhr.open( "GET", `./${ param.value.content_id }/info.json?${ param.value.query_string || Math.floor( Date.now() / 10000 ) }` );
    xhr.onload = json => {
      parent.postMessage({
        iframe_id: param.value.iframe_id,
        type: "aspect",
        value: json.target.response.aspect
      }, "*" );
      
      ReactDOM.render(
        <Player store={{
          info: json.target.response,
          query_string: param.value.query_string,
          iframe_id: param.value.iframe_id
        }}/>,
        document.getElementById( "app" )
      );
    };
    xhr.responseType = "json";
    xhr.send();
    break;
  }

}, false );
