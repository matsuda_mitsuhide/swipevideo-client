import React from "react";
import T from "prop-types";
import replayIcon from "../images/replay.png";
import closeIcon from "../images/close.png";

import {
  Root,
  Title,
  BtnFrame,
  Btn,
  Label,
  Replay,
  Close
} from "./styled/ended.js";

module.exports = React.createClass({
  propTypes: {
    store: T.object.isRequired,
    fullscreen: T.bool.isRequired,
    onToggleFullscreen: T.func.isRequired,
    onReplay: T.func.isRequired
  },
  render() {
    const {
      store,
      fullscreen,
      onToggleFullscreen,
      onReplay
    } = this.props;

    const {
      name,
      id,
    } = store.info;

    return (
      <Root>

        <Title>{ name || "No title" }</Title>

        <BtnFrame>
          <Btn onClick={ onReplay }>
            <Label src={ replayIcon }/>
            { window.innerWidth < 155 ? null : <Replay>Replay</Replay> }
          </Btn>

          {
            fullscreen
            ? <Btn onClick={ onToggleFullscreen }>
                <Label src={ closeIcon }/>
                { window.innerWidth < 155 ? null : <Close>Close</Close> }
              </Btn>
            : null
          }
        </BtnFrame>
      </Root>
    );
  }
});
