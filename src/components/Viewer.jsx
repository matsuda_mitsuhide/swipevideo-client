import React from "react";
import T from "prop-types";
import styled, { keyframes } from "styled-components";
import { osType } from "../helper.js";

import SwipeArea from "./SwipeArea.jsx";
import Controls from "./Controls.jsx";
import Ended from "./Ended.jsx";

const Frame = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

module.exports = React.createClass({
  handleWheel( e ) {
    e.preventDefault();
  },

  handleSeek( controlsCurrentTime ) {
    this.setState({
      controlsCurrentTime,
      seekedTime: controlsCurrentTime
    });
  },
  
  postFullscreenMessage( bool ) {
    parent.postMessage({
      type: bool ? "onFullscreen" : "offFullscreen",
      iframe_id: this.props.store.iframe_id
    }, "*" );
    return bool;
  },

  handleTogglePlaying() {
    this.setState({
      fullscreen: this.postFullscreenMessage( !osType.isPC() && !this.state.playing ? true : this.state.fullscreen ),
      playing: !this.state.playing
    });
  },

  handleToggleFullscreen() {
    this.setState({
      fullscreen: this.postFullscreenMessage( !this.state.fullscreen )
    });
  },

  handleToggleMuted() {
    this.props.audio.muted = !this.state.muted;
    this.setState({
      muted: !this.state.muted
    });
  },

  handleEnded() {
    this.setState({
      playing: false,
      ended: true
    });
  },

  handleReplay() {
    this.setState({
      fullscreen: this.postFullscreenMessage( !osType.isPC() ? true : this.state.fullscreen ),
      playing: true,
      replayCounter: this.state.replayCounter + 1,
      ended: false,
      controlsCurrentTime: 0,
      seekedTime: 0
    });
  },

  propTypes: {
    store: T.object.isRequired,
    audio: T.instanceOf( Audio ).isRequired
  },
  getInitialState() {

    return {
      // fullscreen APIのことではない
      fullscreen: this.postFullscreenMessage( true ),

      // 全画面時のアスペクト比を考慮した上での描画エリアのsize
      fullFrameSize: {
        width: null,
        height: null
      },

      // アセットの状態に関係なくユーザーが期待する再生状態
      playing: true,
      
      replayCounter: 0,

      // 最後まで再生が終わった状態
      ended: false,

      // 再生時刻とタイマーバーの描画用。アセット群の再生時刻管理には使わない。
      controlsCurrentTime: 0,

      // ユーザーがシークして指定した再生時刻
      seekedTime: 0,

      // ユーザーが期待するミュート状態
      muted: false,
    };
  },

  handleResize() {

    const param = {
      width: null,
      height: null
    };

    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;

    if ( windowWidth / windowHeight < this.props.store.info.aspect ) {
      param.width = windowWidth;
      param.height = windowWidth / this.props.store.info.aspect;
    } else {
      param.width = windowHeight * this.props.store.info.aspect;
      param.height = windowHeight;
    }

    this.setState({
      fullFrameSize: param
    });

  },

  windowResize() {
    // Androidで横⇒縦の回転時にサイズを正確に取るために実行を遅らせる
    setTimeout( this.handleResize, 0 );
  },

  audioTimeUpdate( e ) {
    this.setState({
      controlsCurrentTime: e.target.currentTime
    });
  },

  componentWillMount() {
    window.addEventListener( "resize", this.windowResize, false );
    this.handleResize();

    this.props.audio.addEventListener( "timeupdate", this.audioTimeUpdate, false );
  },

  componentWillUnmount() {
    window.removeEventListener( "resize", this.windowResize, false );

    this.props.audio.removeEventListener( "timeupdate", this.audioTimeUpdate, false );
  },

  render() {
    const { store, audio } = this.props;

    return (
      <Frame onWheel={ this.state.fullscreen && !this.state.ended ? this.handleWheel : null }>

        <SwipeArea
          size={ this.state.fullFrameSize }
          store={ store }
          audio={ audio }
          playing={ this.state.playing }
          replayCounter={ this.state.replayCounter }
          seekedTime={ this.state.seekedTime }
          onEnded={ this.handleEnded }
        />

        <Controls
          currentTime={ this.state.controlsCurrentTime }
          duration={ store.info.duration }
          fullscreen={ this.state.fullscreen }
          playing={ this.state.playing }
          muted={ this.state.muted }
          onSeek={ this.handleSeek }
          onTogglePlaying={ this.handleTogglePlaying }
          onToggleFullscreen={ this.handleToggleFullscreen }
          onToggleMuted={ this.handleToggleMuted }
        />

        {
        this.state.ended
        ? <Ended
            store={ store }
            fullscreen={ this.state.fullscreen }
            onToggleFullscreen={ this.handleToggleFullscreen }
            onReplay={ this.handleReplay }
          />
        : null
        }
      </Frame>
    );
  }
});
