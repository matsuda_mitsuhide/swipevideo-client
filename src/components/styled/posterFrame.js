import styled from "styled-components";

module.exports = {

  Title: styled.div`
    padding: 5px 10px 10px;
    background: linear-gradient(to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.2) 50%, transparent 100%);
    position: absolute;
    width: 100%;
    top: 0;
    left: 0;
    box-sizing: border-box;
    font-weight: bold;
  `,

  Thumbnail: styled.img`
    width: 100%;
    height: 100%;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
  `,

  PlayBtnArea: styled.div`
    position: absolute;
    top: 0;
    left: 0;
    cursor: pointer;
    width: 100%;
    height: 100%;

    &:hover > div {
      background: rgba( 0, 128, 255, .85 );
    }
  `,

  SquareBtn: styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -32.5px;
    margin-top: -23px;
    width: 65px;
    height: 46px;
    border-radius: 8px;
    transition: .2s;
    background: rgba( 0, 0, 0, .7 );
  `,

  Arrow: styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -10px;
    margin-top: -12px;
    border: solid transparent;
    border-width: 12px 25px;
    border-left-color: #fff;
  `
};
