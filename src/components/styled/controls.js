import styled from "styled-components";

module.exports = {
  Root: styled.div`
    position: fixed;
    width: 100%;
    left: 0;
    bottom: 0;
    transition: .5s linear;
    padding-bottom: 5px;
    background: linear-gradient(to top, rgba(0, 0, 0, 0.4) 0%, rgba(0, 0, 0, 0.2) 50%, transparent 100%);
  `,

  SeekBarFrame: styled.div`
    position: relative;
    height: 10px;
    margin: 0 10px 2px;

    & > div {
      height: 40%;
      top: 30%;
      opacity: .6;

    }
    &:hover > div {
      height: 60%;
      top: 20%;
      opacity: 1;
    }
  `,

  SeekBar: styled.div`
    position: absolute;
    left: 0;
    right: 0;
    transition: .1s;
    background: rgba( 255, 255, 255, .4 );
  `,

  Progress: styled.div`
    height: 100%;
    position: absolute;
    top: 0;
    background: #f12b24;
    left: 0;
    width: ${ props => props.currentTime / props.duration * 100 }%;
  `,

  SeekableArea: styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    cursor: pointer;
  `,

  SwitchesFrame: styled.div`
    overflow: hidden;
    position: relative;
    margin: 0 5px;
  `,

  Switch: styled.div`
    float: ${ props => props.float };
    cursor: pointer;
    padding: 5px;
    opacity: .8;
    &:hover {
      opacity: 1;
    }
  `,

  Icon: styled.img`
    width: 24px;
    vertical-align: bottom;
  `,

  Timer: styled.div`
    position: absolute;
    fontSize: 10px;
    top: 50%;
    left: 40px;
    transform: translateY(-50%);
  `
};
