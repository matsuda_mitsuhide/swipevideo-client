import styled, { keyframes } from "styled-components";

const fadeinRootAnimation = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

module.exports = {
  Root: styled.div`
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: rgba( 255, 255, 255, .25 );
    animation: ${ fadeinRootAnimation } .3s ease-in-out;
    text-align: center;
  `,

  Title: styled.div`
    padding: 5px 10px 10px;
    background: linear-gradient(to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.2) 50%, transparent 100%);
    position: absolute;
    width: 100%;
    top: 0;
    left: 0;
    box-sizing: border-box;
    font-weight: bold;
  `,
  
  BtnFrame: styled.div`
    position: absolute;
    top: 50%;
    left: 0;
    width: 100%;
    transform: translateY(-50%);
  `,

  Btn: styled.div`
    display: inline-block;
    margin: 8px 10px;
    border-radius: 5px;
    padding: 5px 8px;
    transition: .2s;
    cursor: pointer;
    background: rgba( 0, 0, 0, .75 );
    &:hover {
      background: rgba( 0, 128, 255, .75 );
    }
  `,

  Label: styled.img`
    width: 40px;
    margin-bottom: -3px;
  `,

  Replay: styled.span`
    line-height: 46px;
    vertical-align: bottom;
    font-size: 16px;
    margin-left: 2px;
    margin-right: 10px;
  `,

  Close: styled.span`
    line-height: 46px;
    vertical-align: bottom;
    font-size: 16px;
    margin-left: 2px;
    margin-right: 20px;
  `

};
