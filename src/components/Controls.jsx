import React from "react";
import T from "prop-types";
import { timeToText } from "../helper.js";
import {
  Root,
  SeekBarFrame,
  SeekBar,
  Progress,
  SeekableArea,
  SwitchesFrame,
  Switch,
  Icon,
  Timer
} from "./styled/controls.js";

import onFullImage from "../images/onFull.png";
import offFullImage from "../images/offFull.png";
import playImage from "../images/playArrow.png";
import pauseImage from "../images/pause.png";
import onSoundImage from "../images/onSound.png";
import offSoundImage from "../images/offSound.png";

module.exports = React.createClass({
  propTypes: {
    currentTime: T.number.isRequired,
    duration: T.number.isRequired,
    fullscreen: T.bool.isRequired,
    playing: T.bool.isRequired,
    muted: T.bool.isRequired,
    onSeek: T.func.isRequired,
    onTogglePlaying: T.func.isRequired,
    onToggleFullscreen: T.func.isRequired,
    onToggleMuted: T.func.isRequired,
  },
  handleSeek( e ) {
    this.props.onSeek( ( e.clientX - e.target.getBoundingClientRect().left ) / e.target.clientWidth * this.props.duration );
  },
  render() {
    const {
      currentTime,
      duration,
      fullscreen,
      playing,
      muted,
      onSeek,
      onTogglePlaying,
      onToggleFullscreen,
      onToggleMuted,
    } = this.props;

    return (
      <Root>
        <SeekBarFrame>
          <SeekBar>
            <Progress currentTime={ currentTime } duration={ duration }/>
          </SeekBar>
          <SeekableArea onClick={ this.handleSeek }/>
        </SeekBarFrame>

        <SwitchesFrame>
          <Switch float="left" onClick={ onTogglePlaying }>
            <Icon src={ playing ? pauseImage : playImage }/>
          </Switch>

          {
          window.innerWidth < 180
          ? null
          : <Timer>{ `${ timeToText( currentTime ) }/${ timeToText( duration ) }` }</Timer>
          }


          <Switch float="right" onClick={ onToggleFullscreen }>
            <Icon src={ fullscreen ? offFullImage : onFullImage }/>
          </Switch>

          {
          window.innerWidth < 140
          ? null
          : <Switch float="right" onClick={ onToggleMuted }>
              <Icon src={ muted ? offSoundImage : onSoundImage }/>
            </Switch>
          }

        </SwitchesFrame>
      </Root>
    );
  }
});
