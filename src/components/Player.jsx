import React from "react";
import T from "prop-types";
import styled from "styled-components";

import PosterFrame from "./PosterFrame.jsx";
import Viewer from "./Viewer.jsx";

const Root = styled.div`
  font:12px/1.7em Noto Sans CJK JP, 'Open Sans' , sans-serif;
  color: #fff;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba( 0, 0, 0, .85 );
  overflow: hidden;
`;

module.exports = React.createClass({
  handlePlay() {
    const store = this.props.store;
    
    // モバイル環境で音声を再生させるためにここのユーザーイベントでaudio生成
    const audioElem = new Audio;
    audioElem.volume = .5;
    audioElem.src = `./${ store.info.id }/${ store.info.images[ 0 ] }/audio/audio.mp3${ store.query_string ? "?" + store.query_string : "" }`;
    audioElem.load();

    this.setState({
      played: true,
      audioElem
    });
  },
  propTypes: {
    store: T.object.isRequired
  },
  getInitialState() {
    return {
      played: false,
      audioElem: null
    };
  },
  render() {
    return (
      <Root>
        {
        this.state.played
        ? <Viewer store={ this.props.store } audio={ this.state.audioElem }/>
        : <PosterFrame store={ this.props.store } onPlay={ this.handlePlay }/>
        }
      </Root>
    );
  }
});
