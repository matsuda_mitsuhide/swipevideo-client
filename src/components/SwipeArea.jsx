import React from "react";
import T from "prop-types";
import styled from "styled-components";

import DrawArea from "./DrawArea.jsx";
import SwipeIcon from "./SwipeIcon.jsx";

const Frame = styled.div`
  width: ${ props => props.size.width }px;
  height: ${ props => props.size.height }px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
`;

const SwipeablePlane = styled.div`
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  cursor: ew-resize;
  position: ${ props => props.swiping ? "fixed" : "absolute" };
  z-index: ${ props => props.swiping ? 1000000 : 0 };
`;

module.exports = React.createClass({
  handleSwipeStart( e ) {
    e.preventDefault();
    this.setState({
      swiping: true,
      prevSwipePosX: null
    });
  },
  handleSwipe( e ) {
    if ( !this.state.swiping ) return;

    e.preventDefault();
    e.stopPropagation();

    const x = typeof e.clientX === "number" ? e.clientX : e.touches[ 0 ].pageX;

    if ( this.state.prevSwipePosX === null ) {
      this.setState({
        prevSwipePosX: x
      });
    } else if ( Math.abs( x - this.state.prevSwipePosX ) >= 15 ) {

      let swipedIndex = this.state.swipedIndex;

      swipedIndex += x - this.state.prevSwipePosX > 0 ? -1 : 1;

      if ( swipedIndex === this.props.store.info.images.length ) {
        swipedIndex = 0;
      } else if ( swipedIndex < 0 ) {
        swipedIndex += this.props.store.info.images.length;
      }

      this.setState({
        prevSwipePosX: x,
        swipedIndex
      });

    }

  },
  handleSwipeEnd() {
    this.setState({
      swiping: false
    });
  },
  propTypes: {
    size: T.objectOf( T.number.isRequired ).isRequired,
    store: T.object.isRequired,
    audio: T.instanceOf( Audio ).isRequired,
    seekedTime: T.number.isRequired,
    replayCounter: T.number.isRequired,
    playing: T.bool.isRequired,
    onEnded: T.func.isRequired
  },
  getInitialState() {
    return {
      swiping: false,
      swipedIndex: 0,
      prevSwipePosX: null
    };
  },
  componentDidMount() {
    let keyDowned = false;
    let keyCode = null;
    
    const loop = () => {
      if ( !keyDowned ) return;
      let swipedIndex = this.state.swipedIndex;

      swipedIndex += keyCode === 37 ? -1 : 1;

      if ( swipedIndex === this.props.store.info.images.length ) {
        swipedIndex = 0;
      } else if ( swipedIndex < 0 ) {
        swipedIndex += this.props.store.info.images.length;
      }

      this.setState({
        swipedIndex
      });
    };
    const keydownHandler = e => {
      if ( !keyDowned && ( e.keyCode === 39 || e.keyCode === 37 ) ) {
        keyCode = e.keyCode;
        keyDowned = true;
        this.handleSwipeStart( e );
      }
    };
    const keyupHandler = () => {
      if ( keyDowned ) {
        keyDowned = false;
        keyCode = null;
        this.handleSwipeEnd();
      }
    };
    
    window.addEventListener( "keydown", keydownHandler, false );
    window.addEventListener( "keyup", keyupHandler, false );
    setInterval( loop, 80 );
    
    this.componentWillUnmount = () => {
      clearInterval( loop );
      window.removeEventListener( "keydown", keyHandler, false );
      window.removeEventListener( "keyup", keyupHandler, false );
    };
  },
  render() {
    const { size, store, audio, seekedTime, playing, replayCounter, onEnded } = this.props;

    return (
      <div>
        <Frame size={ size }>
          <DrawArea
            store={ store }
            audio={ audio }
            seekedTime={ seekedTime }
            onEnded={ onEnded }
            swiping={ this.state.swiping }
            swipedIndex={ this.state.swipedIndex }
            playing={ playing }
            replayCounter={ replayCounter }
          />

          <SwipeIcon/>

        </Frame>

        <SwipeablePlane swiping={ this.state.swiping }
          onTouchStart={ this.handleSwipeStart }
          onMouseDown={ this.handleSwipeStart }
          onTouchMove={ this.handleSwipe }
          onMouseMove={ this.handleSwipe }
          onTouchEnd={ this.handleSwipeEnd }
          onMouseUp={ this.handleSwipeEnd }
        />
      </div>
    );
  }
});
