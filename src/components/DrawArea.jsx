import React from "react";
import T from "prop-types";
import styled from "styled-components";
import ResourceUpdater from "../core/ResourceUpdater.js";
import LoadingIcon from "./LoadingIcon.jsx";

const Root = styled.div`
  overflow: hidden;
  position: relative;
  height: 100%;
`;

module.exports = React.createClass({
  handleLoad( bool ) {
    this.setState({
      loading: bool
    });
  },
  propTypes: {
    store: T.object.isRequired,
    audio: T.instanceOf( Audio ).isRequired,
    seekedTime: T.number.isRequired,
    onEnded: T.func.isRequired,
    swiping: T.bool.isRequired,
    swipedIndex: T.number.isRequired,
    replayCounter: T.number.isRequired,
    playing: T.bool.isRequired
  },
  styles: {
    image: {
      width: "100%",
      position: "absolute",
      top: 0,
      left: 0,
      backfaceVisibility: "hidden" // for GPU rendering
    }
  },
  resourceUpdater: null,
  componentDidMount() {
    
    this.resourceUpdater = new ResourceUpdater({
      audio: this.props.audio,
      image: this.refs.image,
      store: this.props.store,
      onChangeLoading: this.handleLoad,
      onEnded: this.props.onEnded
    });
  },
  componentDidUpdate() {
    this.resourceUpdater.setState({
      playing: this.props.playing,
      replayCounter: this.props.replayCounter,
      swiping: this.props.swiping,
      swipedIndex: this.props.swipedIndex,
      loading: this.state.loading,
      seekedTime: this.props.seekedTime
    });
  },
  componentWillUnmount() {
    this.resourceUpdater.dispose();
  },
  getInitialState() {
    return {
      loading: false,
    };
  },
  render() {
    // muted属性がHTMLに反映されてない気がする。今のところ問題はないが
    return (
      <Root>
        <img style={ this.styles.image } ref="image"/>
        <LoadingIcon visible={ this.state.loading }/>
      </Root>
    );
  }
});
