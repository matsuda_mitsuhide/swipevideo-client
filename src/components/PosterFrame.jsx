import React from "react";
import T from "prop-types";

import {
  Title,
  Thumbnail,
  PlayBtnArea,
  SquareBtn,
  Arrow
} from "./styled/posterFrame.js";

module.exports = React.createClass({
  propTypes: {
    store: T.object.isRequired,
    onPlay: T.func.isRequired
  },
  getInitialState() {
    const store = this.props.store;

    return {
      src: `./${ store.info.id }/${ store.info.images[ 0 ] }/image/1.jpg${ store.query_string ? "?" + store.query_string : "" }`,
      titleText: store.info.name || "No title"
    };
  },
  render() {
    const { store, onPlay } = this.props;

    return (
      <div>
        <Thumbnail src={ this.state.src }/>

        <Title>{ this.state.titleText }</Title>

        <PlayBtnArea onClick={ onPlay }>
          <SquareBtn>
            <Arrow/>
          </SquareBtn>
        </PlayBtnArea>

      </div>
    );
  }
});
