import React from "react"
import styled, { keyframes } from "styled-components";

import swipeImage from "../images/swipe.png";
const slideSwipeIcon = keyframes`
  from {
    left: 25%;
  }
  to {
    left: 75%;
  }
`;
const fadeoutSwipeIcon = keyframes`
  from {
    opacity: 1;
  }
  to {
    opacity: 0;
  }
`;
const Root = styled.img`
  position: absolute;
  bottom: 60px;
  left: 25%;
  width: 50px;
  margin-left: -25px;
  animation:
    ${ slideSwipeIcon } 1.5s ease-in-out .5s 5 alternate,
    ${ fadeoutSwipeIcon } 1s ease-in-out 6.5s forwards;
`;

module.exports = React.createClass({
  render() {
    return (
      <Root src={ swipeImage }/>
    );
  }
});
