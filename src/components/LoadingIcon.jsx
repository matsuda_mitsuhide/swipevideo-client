import React from "react";
import T from "prop-types";
import styled, { keyframes } from "styled-components";
const CIRCLE_COUNT = 7;

const spinCircles = keyframes`
  from {
    transform: rotate(360deg);
  }
  to {
    transform: rotate(0deg);
  }
`;

const Root = styled.div`
  opacity: ${ props => props.visible ? 1 : 0 };
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: -30px;
  margin-left: -30px;
  width: 60px;
  height: 60px;
  transition: opacity 0s ease ${ props => props.visible ? .5 : 0 }s;
  animation: ${ spinCircles } 1s linear infinite;
`;

const CircleFrame = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  transform: rotate(${ props => Math.PI * 2 / CIRCLE_COUNT * props.index }rad);
  opacity: ${ props => ( CIRCLE_COUNT - props.index * .6 ) / CIRCLE_COUNT };
`;

const Circle = styled.div`
  position: absolute;
  top: 50%;
  left: 100%;
  margin-top: -8px;
  margin-left: -8px;
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background: #aaa;
`;

module.exports = React.createClass({
  propTypes: {
    visible: T.bool.isRequired
  },
  render() {
    return (
      <Root visible={ this.props.visible }>
        { Array.apply( null, { length: CIRCLE_COUNT } ).map( ( e, index ) =>
          <CircleFrame key={ index } index={ index }>
            <Circle/>
          </CircleFrame>
          )
        }
      </Root>
    );
  }
});
