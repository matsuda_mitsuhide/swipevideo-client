const osType = {
  isIOS: () => /iphone|ipod|ipad/.test( navigator.userAgent.toLowerCase() ),
  isAndroid: () => /android/.test( navigator.userAgent.toLowerCase() ),
  isPC: () => !osType.isIOS() && !osType.isAndroid() && !/mobile/.test( navigator.userAgent.toLowerCase() )
};

module.exports = {
  timeToText: time => ( "00" + Math.floor( time / 60 ) ).slice( -2 ) + ":" + ( "00" + Math.floor( time % 60 ) ).slice( -2 ),
  ImageBufferLoader: class {
    constructor( path, onLoad ) {
      const start = Date.now();
      const xhr = new XMLHttpRequest;
      xhr.open( "GET", path, true );
      xhr.responseType = "arraybuffer";
      xhr.onload = e => {
        this.delay = Date.now() - start;
        this.objectURL = URL.createObjectURL( new Blob( [ e.target.response ], { type: "image/jpg" } ) );
        onLoad( this );
        onLoad = null;
      };
      xhr.send( null );
    }
  },
  osType,
  log: function() {
    let input = null;
    return value => {
      if ( osType.isPC() ) {
        console.log( value );
      } else {
        if ( input === null ) {
          input = document.createElement( "input" );
          input.style.position = "fixed";
          input.style.top = "10px";
          input.style.left = "10px";
          input.style.zIndex = 100000;
          document.body.appendChild( input );
        }
        input.value = value;
      }
    };
  }()
};
