const VERSION = "v0.12.0";
let connect_path = null;
const iframeCache = [];

const swipevideo = {
  connect: path => connect_path = path,
  version: VERSION,
  attach: ( elem, content_id, query_string ) => {
    const index = iframeCache.length;
    elem.classList.add( "swipevideo-isinit" );
    
    const iframe = document.createElement( "iframe" );
    iframe.style.width = "100%";
    iframe.style.height = "100%";
    iframe.style.display = "block";
    iframe.style.top = 0;
    iframe.style.left = 0;
    iframe.setAttribute( "scrolling", "no" );
    iframe.setAttribute( "allowtransparency", "true" );
    iframe.setAttribute( "frameborder", "none" );
    
    iframeCache[ index ] = iframe;
    
    const heightDiv = document.createElement( "div" );
    heightDiv.style.position = "relative";
    iframe.style.position = "absolute";
    elem.appendChild( heightDiv );
    heightDiv.appendChild( iframe );
    
    iframe.addEventListener( "load", e => {
      e.target.contentWindow.postMessage({
        type: "init",
        value: {
          iframe_id: index,
          content_id,
          query_string
        }
      }, "*" );
    }, false );
    iframe.src = connect_path;
    
    return iframe;
  },
  detach: elem => {
    elem.classList.remove( "swipevideo-isinit" );

    const index = iframeCache.indexOf( elem.querySelector( "iframe" ) );
    const iframe = iframeCache[ index ];
    iframe.contentWindow.postMessage({
      type: "detach"
    }, "*" );
    
    iframeCache[ index ] = null;
    delete iframeCache[ index ];
  },
  embed: () => {

    document.querySelectorAll( ".swipevideo-player" ).forEach( target => {
      if ( /swipevideo-isinit/.test( target.className ) ) return;
      
      swipevideo.attach( target, target.getAttribute( "data-id" ) );
    });

  }
};

window.addEventListener( "DOMContentLoaded", swipevideo.embed, false );
window.addEventListener( "message", e => {

  if ( !( new RegExp( "^" + e.origin + "(\/|$)" ) ).test( connect_path ) && e.origin !== location.origin ) return;
  
  const param = e.data;
  const iframe = iframeCache[ param.iframe_id ];

  switch( param.type ) {
  case "onFullscreen":
    iframe.style.position = "fixed";
    iframe.style.zIndex = 1000000;
    break;
  case "offFullscreen":
    iframe.style.position = "absolute";
    iframe.style.zIndex = 0;
    break;
  case "aspect":
    iframe.parentNode.style.paddingTop = 1 / param.value * 100 + "%";
    break;
  }
}, false );

console.log( `SwipeVideoPlayer ${ VERSION }` );

module.exports = swipevideo;
